package main

import (
	"fmt"

	"shamir/internal/pkg/cmd"
)

func main() {
	fmt.Printf("Shamir's Secret Sharing app\n\n")
	cmd.Execute()
}
