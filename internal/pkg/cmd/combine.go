package cmd

import (
	"fmt"
	"log"
	"shamir/internal/pkg/shamir"
	"strings"

	"github.com/spf13/cobra"
)

var combineCmd = &cobra.Command{
	Use:   "combine [share1] [share2] [share3]",
	Short: "Combines shares and restore the secret",
	Run: func(cmd *cobra.Command, shares []string) {
		fmt.Printf("🍀 Combine shares %s\n", strings.Join(shares, " "))
		result, err := shamir.CombineStr(shares)
		if err != nil {
			log.Fatalf("can't combine shares: %v", err)
		}

		fmt.Printf("Your secret: `%s`\n", result)
	},
}

func init() {
	RootCmd.AddCommand(combineCmd)
}
