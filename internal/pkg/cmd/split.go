package cmd

import (
	"fmt"
	"log"
	"shamir/internal/pkg/shamir"
	"strings"

	"github.com/spf13/cobra"
)

var (
	shares    int16
	threshold int16
)

var splitCmd = &cobra.Command{
	Use:   "split [secret]",
	Short: "Splits secret to shares",
	Run: func(cmd *cobra.Command, args []string) {
		secret := strings.Join(args, " ")
		fmt.Printf("🦄 Share secret `%s` with %d people with %d threshold\n", secret, shares, threshold)
		shares, err := shamir.SplitStr(secret, int64(shares), int64(threshold))
		if err != nil {
			log.Fatalf("split error: %v", err)
		}

		fmt.Printf("Your shares: %s\n", strings.Join(shares, " "))
	},
}

func init() {
	RootCmd.AddCommand(splitCmd)

	splitCmd.Flags().Int16VarP(&threshold, "threshold", "t", 3, "min shares to reconstruct the secret")
	splitCmd.Flags().Int16VarP(&shares, "shares", "s", 5, "the number of shares")
}
