package shamir

import "fmt"

func combine(points []Point, modulus int64) int64 {
	return lagrange(points, modulus)
}

func CombineStr(sharesStr []string) (string, error) {
	if err := verifyShares(sharesStr); err != nil {
		return "", fmt.Errorf("verify: %v", err)
	}

	shares := make([]Share, len(sharesStr))
	for i := range sharesStr {
		shares[i] = Share(hexToSlice(sharesStr[i]))
	}

	secretLen := len(shares[0]) - 1 // 1 byte is reserved for share number, other bytes is the secret itself
	combinedBytes := make([]int64, secretLen)

	points := make([]Point, len(shares))

	for i := 0; i < secretLen; i++ { // i - byte number of secret
		for j := range shares { // j - share number
			points[j] = Point{
				x: int(shares[j][0] + 1),
				y: shares[j][i+1], // i-th byte of j-th share
			}
		}

		combinedBytes[i] = combine(points, int64(prime))
	}

	return binToStr(combinedBytes), nil
}
