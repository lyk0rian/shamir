package shamir

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestShamir_splitFixed(t *testing.T) {
	// the values was taken from https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing
	secret := int64(1234)
	modulus := int64(1613)
	n := int64(6) // number of shares
	coefficients := []int64{secret, 166, 94}
	expected := []int64{1494, 1942, 2578, 3402, 4414, 5614}

	for i := range expected {
		expected[i] = expected[i] % modulus
	}

	result := splitFixed(secret, modulus, n, coefficients)
	require.Equal(t, expected, result)
}

func TestShamir_evaluatePolynomial(t *testing.T) {
	tests := []struct {
		coefficients         []int64
		x, modulus, expected int64
	}{
		{
			coefficients: []int64{2, 4, 3, 0, 2},
			x:            3,
			modulus:      17,
			expected:     16,
		},
		{
			coefficients: []int64{-3, 12, -9, 0, 1},
			x:            -5,
			modulus:      20,
			expected:     17,
		},
	}

	for _, tt := range tests {
		result := evaluatePolynomial(tt.x, tt.modulus, tt.coefficients)
		require.Equal(t, tt.expected, result)
	}

	coefficients := []int64{1234, 166, 94}
	modulus := int64(1613)
	// the values was taken from https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing
	expected := []int64{1234, 1494, 1942, 2578, 3402, 4414, 5614}

	for i, value := range expected {
		result := evaluatePolynomial(int64(i), modulus, coefficients)
		require.Equal(t, value%modulus, result)
	}
}
