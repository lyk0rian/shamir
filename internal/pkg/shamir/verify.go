package shamir

import "fmt"

func verifySecret(value []byte) error {
	if len(value) == 0 {
		return fmt.Errorf("secret not indicated")
	}

	if len(value) > 50 {
		return fmt.Errorf("secret too long")
	}

	for i := 0; i < len(value); i++ {
		if value[i] > 253 {
			return fmt.Errorf("invalid character `%v` at position %d", value[i], i)
		}
	}

	return nil
}

func verifyShares(shares []string) error {
	if len(shares) < 2 {
		return fmt.Errorf("to few shares [%d]", len(shares))
	}

	seen := map[string]struct{}{}
	shareLen := len(shares[0])
	for _, share := range shares {
		if shareLen != len(share) {
			return fmt.Errorf("shares should have the same lenght, share `%s` is invalid", share)
		}

		if _, exists := seen[share]; exists {
			return fmt.Errorf("share `%s` is duplicate", share)
		}

		seen[share] = struct{}{}
	}

	return nil
}
