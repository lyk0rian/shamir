package shamir

// Evaluates polynomial at x using Horner's method.
// https://www.geeksforgeeks.org/horners-method-polynomial-evaluation/
func evaluatePolynomial(x, modulus int64, coefficients []int64) int64 {
	degree := len(coefficients) - 1
	y := coefficients[degree]
	for i := degree - 1; i >= 0; i-- {
		y = y*x + coefficients[i]
	}
	return (y + modulus) % modulus
}
