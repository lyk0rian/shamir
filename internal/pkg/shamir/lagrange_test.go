package shamir

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestShamir_Lagrange(t *testing.T) {
	// the data was taken from https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing
	points := []Point{
		{1, 1494},
		{2, 1942},
		{3, 2578},
		{4, 3402},
		{5, 4414},
		{6, 5614},
	}

	for i := 0; i < 6; i++ {
		points2 := getNpoints(points, 3)
		result := lagrange(points2, 1399)
		require.Equalf(t, int64(1234), result, "can't restore secret from points: %+v", points2)
	}
}

func TestShamir_ModInverse(t *testing.T) {
	tests := []struct {
		a, m, expected int64
	}{
		{3, 11, 4},
		{42, 2017, 1969},
		{-2, 1399, 699},
		{-4, 1399, 1049},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("modinverse_for_%d_and_%d", tt.a, tt.m), func(t *testing.T) {
			result := modInverse(int64(tt.a), int64(tt.m))
			require.Equal(t, int64(tt.expected), result)
		})
	}
}
