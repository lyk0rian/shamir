package shamir

import (
	"fmt"
	"math/rand"
	"time"
)

var prime = 253

type Share []int64

func SplitStr(secret string, n, threshold int64) (shares []string, err error) {
	if err = verifySecret([]byte(secret)); err != nil {
		return nil, fmt.Errorf("verify: %v", err)
	}

	shares = splitStr(secret, n, threshold)
	result, err := CombineStr(shares)
	if err != nil {
		return nil, fmt.Errorf("check: combine error: %v", shares)
	}

	if result != secret {
		return nil, fmt.Errorf("check: can't split secret")
	}

	return shares, nil
}

func splitStr(secret string, n, threshold int64) []string {
	secretBytes := strToBin(secret)
	modulus := int64(prime)

	// we use 0 byte for keeping the share number, other bytes are used for keeping the share itself.
	shares := make([]Share, n)

	for i := range secretBytes { // i - byte number of secret
		var splited []int64
		for k := 0; k < 10000; k++ {
			splited = split(secretBytes[i], modulus, n, threshold)
			points := make([]Point, len(splited))
			for m := range splited {
				points[m] = Point{
					x: m + 1,
					y: splited[m],
				}
			}
			restored := combine(points, int64(prime))
			if restored == secretBytes[i] {
				break
			}

		}

		for j := range splited { // j - share number
			if i == 0 {
				shares[j] = make(Share, 1, len(secretBytes))
				shares[j][0] = int64(j) // store share number to 0 byte of data
			}

			shares[j] = append(shares[j], splited[j])
		}
	}

	result := make([]string, len(shares))
	for i := range shares {
		result[i] = sliceToHex(shares[i])
	}

	return result
}

// split splits secret to n shares. You need at least threshold of shares to restore the secret
func split(secret, modulus, n, threshold int64) []int64 {
	coefficients := randomCoefficients(secret, modulus, threshold-1)
	return splitFixed(secret, modulus, n, coefficients)
}

func splitFixed(secret, modulus, n int64, coefficients []int64) []int64 {
	shares := make([]int64, n)

	for x := int64(0); x < n; x++ {
		shares[x] = evaluatePolynomial(x+1, modulus, coefficients)
	}

	return shares
}

func randomCoefficients(secret, modulus, degree int64) []int64 {
	coefficients := make([]int64, degree+1)
	coefficients[0] = secret // initialize coefficients with a0, see https://en.wikipedia.org/wiki/Shamir%27s_Secret_Sharing

	// generate coefficients
	for i := int64(1); i <= degree; i++ {
		coefficients[i] = nonzeroRandom(modulus)
	}

	return coefficients
}

func nonzeroRandom(modulus int64) (result int64) {
	if modulus < 1 {
		return 1
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for {
		result = r.Int63n(modulus)
		if result != 0 {
			break
		}
	}

	return result
}
