package shamir

type Point struct {
	x int
	y int64
}

// https://www.geeksforgeeks.org/program-to-implement-inverse-interpolation-using-lagrange-formula/
func lagrange(points []Point, modulus int64) (result int64) {
	result = 0

	for i := 0; i < len(points); i++ {
		prod := int64(1)
		for j := 0; j < len(points); j++ {
			if i == j {
				continue
			}
			d := int64(points[j].x - points[i].x) // d[i].y - d[j].y
			d = modInverse(d, modulus)
			d = d * int64(points[j].x)
			prod = prod * d
			prod = prod % modulus
		}
		term := points[i].y * prod
		result += term
	}
	return result % modulus
}

// modInverse finds modulo inverse of a
func modInverse(a, m int64) int64 {
	a = a % m
	var r int64
	if a < 0 {
		xyz := gcdD(m, -a)
		r = -xyz[2]
	} else {
		xyz := gcdD(m, a)
		r = xyz[2]
	}

	return (m + r) % m
}

func gcdD(a, b int64) *[3]int64 {
	var xyz [3]int64

	if b == 0 {
		xyz[0] = a
		xyz[1] = 1
		xyz[2] = 0
	} else {
		n := int64(a / b)
		c := a % b
		r := gcdD(b, c)

		xyz[0] = r[0]
		xyz[1] = r[2]
		xyz[2] = r[1] - r[2]*n
	}

	return &xyz
}
