package shamir

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestShamir_SplitCombineOneNumber(t *testing.T) {
	for i := 1; i < 100; i++ {
		t.Run(fmt.Sprintf("pass #%d", i), func(t *testing.T) {
			splitCombineOneNumber(t)
		})
	}
}
func splitCombineOneNumber(t *testing.T) {
	modulus := int64(253)
	// modulus := int64(1613)
	n := int64(6)
	threshold := int64(3)

	for secret := int64(0); secret < modulus; secret++ {
		shares := split(secret, modulus, n, threshold)

		points := make([]Point, len(shares))
		for i := range shares {
			if shares[i] > 255 {
				t.Logf("got byte %d", shares[i])
				t.Fail()
			}
			points[i] = Point{
				x: i + 1,
				y: shares[i],
			}
		}

		for i := 0; i < 10; i++ {
			points2 := getNpoints(points, int(threshold))
			result := combine(points2, modulus)
			require.Equalf(t, secret, result, "can't restore secret from points: %+v", points2)
		}
	}

}

func TestShamir_SplitCombine(t *testing.T) {
	tests := []struct {
		secret, modulus, n, threshold int64
	}{
		{
			secret:    1234,
			modulus:   1613,
			n:         6,
			threshold: 3,
		},
		{
			secret:    42,
			modulus:   253,
			n:         6,
			threshold: 3,
		},
	}

	for _, tt := range tests {
		shares := split(tt.secret, tt.modulus, tt.n, tt.threshold)

		points := make([]Point, len(shares))
		for i := range shares {
			points[i] = Point{
				x: i + 1,
				y: shares[i],
			}
		}

		for i := 0; i < int(tt.n); i++ {
			points2 := getNpoints(points, int(tt.threshold))
			result := combine(points2, tt.modulus)
			require.Equalf(t, tt.secret, result, "can't restore secret from points: %+v", points2)
		}
	}

}

func TestShamir_IntStrConversion(t *testing.T) {
	str := "some test string с русскими буквами"
	ints := strToInts(str)
	str2 := intsToStr(ints)
	require.Equal(t, str, str2)

	bin := strToBin(str)
	str3 := binToStr(bin)
	require.Equal(t, str, str3)
}

func TestShamir_SplitCombineStr(t *testing.T) {
	secret := "message с русскими буквами"
	n := int64(10)
	threshold := int64(3)

	shares, err := SplitStr(secret, n, threshold)

	require.NoError(t, err)

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	shareIndexes := r.Perm(len(shares))[:threshold]

	shares2 := make([]string, len(shareIndexes))
	for i := range shareIndexes {
		shares2[i] = shares[shareIndexes[i]]
	}

	result, err := CombineStr(shares2)
	require.NoError(t, err)
	require.Equal(t, secret, result)
}

func TestShamir_SplitCombineStr_raw(t *testing.T) {
	secret := "top secret message"
	secretBytes := strToBin(secret)
	modulus := int64(253)
	n := int64(10)
	threshold := int64(4)

	// we use 0 byte for share number, other bytes is the share itself.
	shares := make([]Share, n)

	for i := range secretBytes { // i - byte number of secret
		splited := split(secretBytes[i], modulus, n, threshold)
		for j := range splited { // j - share number
			if i == 0 {
				shares[j] = make(Share, 1, len(secretBytes))
				shares[j][0] = int64(j) // store share number to 0 byte of data
			}

			shares[j] = append(shares[j], splited[j])
		}
	}

	secretLen := len(shares[0]) - 1 // 1 byte is reserved for share number, other bytes is the secret itself
	combinedBytes := make([]int64, secretLen)

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	shareIndexes := r.Perm(len(shares))[:threshold]

	points := make([]Point, len(shareIndexes))

	for i := 0; i < secretLen; i++ { // i - byte number of secret
		for j := range shareIndexes { // j - share number
			points[j] = Point{
				x: int(shares[j][0] + 1),
				y: shares[j][i+1], // i-th byte of j-th share
			}
		}

		combinedBytes[i] = combine(points, modulus)
	}

	combined := binToStr(combinedBytes)
	require.Equal(t, secret, combined)
}
