package shamir

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestShamir_getNpoints(t *testing.T) {
	points := []Point{
		{1, 2},
		{3, 4},
		{5, 6},
		{7, 8},
		{9, 10},
		{11, 12},
		{13, 14},
		{15, 16},
		{17, 18},
		{19, 20},
		{21, 22},
	}

	n := 4

	result1 := getNpoints(points, n)
	result2 := getNpoints(points, n)

	require.Len(t, result1, n)
	require.Len(t, result2, n)

	// we should get different points every time
	require.NotEqual(t, result1, result2)

	// the points should NOT repeat
	checkUniqueness(t, result1)
	checkUniqueness(t, result2)
}

func checkUniqueness(t *testing.T, points []Point) {
	exists := map[int]struct{}{}

	for i := range points {
		x := points[i].x
		if _, ok := exists[x]; ok {
			t.Errorf("duplicate point %+v from points: %+v", points[i], points)
		}

		exists[x] = struct{}{}
	}
}

func TestShamir_hexToSlice(t *testing.T) {
	sample := "73616D706C6520737472696E67"
	expected := []int64{0x73, 0x61, 0x6D, 0x70, 0x6C, 0x65, 0x20, 0x73, 0x74, 0x72, 0x69, 0x6E, 0x67}

	require.Equal(t, expected, hexToSlice(sample))
}

func TestShamir_sliceToHex(t *testing.T) {
	sample := []int64{0x73, 0x61, 0x6D, 0x70, 0x6C, 0x65, 0x20, 0x73, 0x74, 0x72, 0x69, 0x6E, 0x67}
	expected := "73616d706c6520737472696e67"

	require.Equal(t, expected, sliceToHex(sample))
}
