package shamir

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

// getNpoints return n random points from the source array
func getNpoints(points []Point, n int) []Point {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	indicies := r.Perm(len(points))
	points2 := make([]Point, n)

	for i := 0; i < n; i++ {
		points2[i] = points[indicies[i]]
	}

	return points2
}

func strToInts(str string) []int64 {
	runes := []rune(str)
	result := make([]int64, len(runes))

	for i := range runes {
		result[i] = int64(runes[i])
	}

	return result
}

func strToBin(str string) []int64 {
	bytes := []byte(str)
	result := make([]int64, len(bytes))

	for i := range bytes {
		result[i] = int64(bytes[i])
	}

	return result
}

func intsToStr(ints []int64) string {
	result := make([]rune, len(ints))

	for i := range ints {
		result[i] = rune(ints[i])
	}

	return string(result)
}

func binToStr(ints []int64) string {
	result := make([]byte, len(ints))

	for i := range ints {
		result[i] = byte(ints[i])
	}

	return string(result)
}

func sliceToHex(arr []int64) string {
	result := make([]string, len(arr))
	for i := range arr {
		result[i] = fmt.Sprintf("%02x", arr[i])
	}

	return strings.Join(result, "")
}

func hexToSlice(str string) []int64 {
	decodedByteArray, err := hex.DecodeString(str)
	if err != nil {
		return nil
	}

	result := make([]int64, len(decodedByteArray))
	for i := range decodedByteArray {
		result[i] = int64(decodedByteArray[i])
	}

	return result
}
